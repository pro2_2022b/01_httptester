package experiments;

import dataModel.BadTestResult;
import dataModel.GoodTestResult;
import dataModel.TestCollection;
import dataModel.TestResult;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Properties;

public class Main
{
    public static void main(String[] args) throws IOException {
        Properties config = new Properties();
        config.load(new FileInputStream("app.config"));
        String[] urls = config.getProperty("urls").split(",");
        int repeatCount = Integer.parseInt(config.getProperty("repeatCount"));

        LinkedList<TestCollection> testCollections = new LinkedList<>();
        for (String url : urls) {
            TestCollection testCollection = new TestCollection(url);
            testCollections.add(testCollection);
            for (int i = 0; i < repeatCount; i++) {
                int httpResponseCode = 0;

                URL url1 = new URL("http://" + url);
                HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
                conn.setRequestMethod("GET");
                httpResponseCode = conn.getResponseCode();

                if (httpResponseCode == 200) {
                    testCollection.Results.add(new GoodTestResult());
                } else {
                    testCollection.Results.add(new BadTestResult(httpResponseCode));
                }
            }
        }

        for (TestCollection testCollection : testCollections)
        {
            for (TestResult result : testCollection.Results)
            {
                System.out.println(result.GetSummary());
            }
        }
    }
}
