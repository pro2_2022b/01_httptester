package experiments;

import dataModel.BadTestResult;
import dataModel.GoodTestResult;
import dataModel.TestCollection;
import dataModel.TestResult;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class Main2
{
    public static void main(String[] args) throws IOException, InterruptedException {

        String data = "127.0.01.64";
        String[] splitted = data.split("\\."); // regex je \.

        Path path = Paths.get(System.getProperty("user.dir"), "output.pro2" );
        FileOutputStream fileOutputStream = new FileOutputStream(path.toString());
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);

        for(int i=0;i<100;i++)
        {
            int elevation = Elevation.RealElevation(48.5f, 17.5f + i * 0.01f);
            System.out.println(elevation);
            dataOutputStream.writeInt(elevation);
            Thread.sleep(200);
        }

        dataOutputStream.flush();
        dataOutputStream.close();
    }
}
