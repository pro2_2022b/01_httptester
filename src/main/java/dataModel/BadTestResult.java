package dataModel;

public class BadTestResult extends TestResult
{
    public BadTestResult(int httpCode)
    {
        this.httpCode = httpCode;
    }

    int httpCode;

    @Override
    public String GetSummary()
    {
        return "Not OK ("+httpCode+")";
    }
}
